<?php
	defined('BASEPATH') OR exit('No direct script access allowed');
	
	class Home extends CI_Controller {
	
		 public function __construct()
	 	{
	 		parent::__construct();
			$this->load->helper('url');
	 		$this->load->model('M_guru');
	 	}

		public function index()
		{
			$data['guru']=$this->M_guru->get_all_guru();
			$this->load->view('index',$data);		
		}

		public function guru_add()
		{
			$data = array(
					'nama_lengkap' => $this->input->post('nama_lengkap'),
					'alamat' => $this->input->post('alamat'),
					'jenis_kelamin' => $this->input->post('jenis_kelamin'),
					'gaji_pokok' => $this->input->post('gaji_pokok'),
					'tunjangan_kinerja' => $this->input->post('tunjangan_kinerja'),
					'presentasi_pengajaran' => $this->input->post('presentasi_pengajaran'),
				);
			$insert = $this->M_guru->guru_add($data);
			echo json_encode(array("status" => TRUE));
		}
		public function ajax_edit($id)
		{
			$data = $this->M_guru->get_by_id($id);



			echo json_encode($data);
		}

			public function guru_update()
		{
			$data = array(
					'nama_lengkap' => $this->input->post('nama_lengkap'),
					'alamat' => $this->input->post('alamat'),
					'book_author' => $this->input->post('book_author'),
					'gaji_pokok' => $this->input->post('gaji_pokok'),
					'tunjangan_kinerja' => $this->input->post('tunjangan_kinerja'),
					'presentasi_pengajaran' => $this->input->post('presentasi_pengajaran'),
				);
			$this->M_guru->guru_update(array('uuid' => $this->input->post('uuid')), $data);
			echo json_encode(array("status" => TRUE));
		}

		public function guru_delete($id)
		{
			$this->M_guru->delete_by_id($id);
			echo json_encode(array("status" => TRUE));
		}


	}
	
	/* End of file Home.php */
	/* Location: ./application/controllers/Home.php */	